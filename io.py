#!/usr/bin/env python

import sys
import os

def main():
    c = read('README.md')
    print(c)

    d = read('IDontExist')
    print(d)

def read(filepath):
    try:
        with open(filepath, 'r') as f:
            content = f.read()
            return content
    except FileNotFoundError:
        return None


if __name__ == '__main__':
    main()
